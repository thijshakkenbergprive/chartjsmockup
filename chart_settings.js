// Color sets for charts

var chartColorGrey = 'rgb(228,228,228)'; //grey

var chartColorBronze ='rgb(191,113,77)';  
var chartColorSilver ='rgb(204,204,204)';
var chartColorGold   ='rgb(242,200,15)';


// // SET 2
// var chartColor1     = 'rgb(232, 79, 116)';      // pink
// var chartColor1bg   = 'rgb(232, 79, 116, 0.3)';     // light pink
// var chartColor2     = 'rgb(159,129,255)';       // purple
// var chartColor2bg   = 'rgb(159,129,255, 0.3)';       // light purple
// var chartColor3     = 'rgb(49,184,221)';        // blue
// var chartColor3bg   = 'rgb(49,184,221,0.3)';
// var chartColor4     = 'rgb(23,219,187)';        // green
// var chartColor4bg   = 'rgb(23,219,187,0.3)'; 
// var chartColor5     = 'rgb(251,241,112)';       // yellow



// SET 5
var chartColor1 = 'rgb(126,78,116)';  // purple
var chartColor1bg = 'rgb(126,78,116, 0.3)';  // purple
var chartColor2 = 'rgb(96,150,220)';   // blue
var chartColor2bg = 'rgb(96,150,220, 0.3)';   // blue
var chartColor3 = 'rgb(209,218,255)';    // light blue
var chartColor3bg = 'rgb(209,218,255, 0.3)';    // light blue
var chartColor4 = 'rgb(0,184,169)';    // green
var chartColor4bg = 'rgb(0,184,169, 0.3)';    // green
var chartColor5 = 'rgb(0,93,103)';   // teal
var chartColor5bg = 'rgb(0,93,103, 0.3)';   // teal


// 6 months pass rate & effectiveness
var chart1 = {
    type: 'line',
    data: {
        labels: [
            'May 2019',
            'June 2019',
            'July 2019',
            'August 2019',
            'September 2019',
            'October 2019'
            ],
        datasets: [{
            label: 'Avg Top Score (proficiency)',
            data: [84, 82, 81, 83, 90, 88],
            borderWidth: 2,
            borderColor: chartColor2,
            backgroundColor: chartColor2bg,
            lineTension:0
        },
        {
            label: 'Pass Rate (effectivity)',
            data: [72, 73, 77, 69, 67, 65],
            borderWidth: 2,
            borderColor: chartColor1,
            backgroundColor: chartColor1bg,
            lineTension:0    
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    max: 100
                }
            }]
        },
        legend:{
            labels:{
                usePointStyle: true,
                fontSize:11,
                boxWidth:6
            }
        },
    }
};

var chart2 = {
    type: 'horizontalBar',
    data: {
        labels: ["Theory", "Practical"],
        datasets: [{
            label: 'Passed',
            data: [727, 589],
            backgroundColor: chartColor1,
            //hoverBackgroundColor: "rgba(50,90,100,1)"
        },{
            label: 'Failed',
            data: [238, 553],
            backgroundColor: chartColorGrey,
           // hoverBackgroundColor: "rgba(140,85,100,1)"
        }]
    },
    options: {
        tooltips: {
            enabled: false
        },
        hover :{
            animationDuration:0
        },
        scales: {
            xAxes: [{
                ticks: {
                    beginAtZero:true,
                    fontSize:11
                },
                scaleLabel:{
                    display:false
                },
                gridLines: {
                }, 
                stacked: true
            }],
            yAxes: [{
                gridLines: {
                    display:false,
                    color: "#fff",
                    zeroLineColor: "#fff",
                    zeroLineWidth: 0
                },
                ticks: {
                    fontSize:11
                },
                stacked: true
            }]
        },
        legend:{
            position:'top',
            display:true,
            labels: {
                usePointStyle: true,
                fontSize:11,
                boxWidth:6
            }
        },
            animation: {
            onComplete: function () {
                var chartInstance = this.chart;
                var ctx = chartInstance.ctx;
                ctx.textAlign = "left";
                ctx.font = "9px";
                ctx.fillStyle = "#fff";

                Chart.helpers.each(this.data.datasets.forEach(function (dataset, i) {
                    var meta = chartInstance.controller.getDatasetMeta(i);
                    Chart.helpers.each(meta.data.forEach(function (bar, index) {
                        data = dataset.data[index];
                        if(i==0){
                            ctx.fillText(data, 50, bar._model.y+4);
                        } else {
                            ctx.fillText(data, bar._model.x-20, bar._model.y+4);
                        }
                    }),this)
                }),this);
            }
        },
    },
}

// current proficiency
var chart3 = {
    type: 'doughnut',
    data: {
        datasets: [{
            label: 'Avg. Top Score',
            data: [50,9],
            text: "000",
            backgroundColor: [
                chartColor2
            ],
            borderWidth: 0
        }],
        labels: [
            'Avg. Top Score %',
        ]
    },
    options: {
        rotation: 1 * Math.PI,
        circumference: 1 * Math.PI,
        cutoutPercentage: 70,
        legend:{
            position:'top',
            labels: {
                usePointStyle: true,
                fontSize:11,
                boxWidth:6
            }
        }
    }    
}

var chart4 = {
    type: 'bar',
    data: {
        labels: [
            'May 2019',
            'June 2019',
            'July 2019',
            'August 2019',
            'September 2019',
            'October 2019'
            ],
        datasets: [
            {
                label: 'Master',
                data: [12,21,4,5,2,4],
                type:'line',
                lineTension:0,
                borderColor: chartColor4,
                backgroundColor: chartColor4bg
            },                                
            {
                label: 'Gold',
                data: [12, 19, 3, 5, 2, 3],
                backgroundColor:chartColorGold,
                borderWidth: 1,
                stack:1
            },
            {
                label: 'Silver',
                data: [3,12,2,1,1,4],
                backgroundColor: chartColorSilver,
                stack:1
            },
            {
                label: 'Bronze',
                data: [21,34,7,12,4,7],
                backgroundColor: chartColorBronze,
                stack:1
            }
        ]
    },
    options: {
        barValueSpacing: 10,
        scales: {
            yAxes: [{
                ticks: {
                }
            }],
            xAxes: [{
                barPercentage:0.4
            }]
        },
        legend:{
            labels:{
                usePointStyle: true,
                fontSize:10,
                boxWidth:6
            }
        },
    }
}

var chart5 = {
    type: 'pie',
    data: {
        labels: ["Gold","Silver","Bronze"],
        datasets: [{
            data: [72,238,144],
            backgroundColor: [chartColorBronze, chartColorSilver, chartColorGold]
        }]
    },
    options: {
        tooltips: {
            enabled: true
        },
        hover :{
            animationDuration:0
        },
        legend:{
            position:'right',
            labels: {
                usePointStyle: true,
                fontSize:11,
                boxWidth:6
            }
        }
    },
}

// master medals %
var chart6 = {
    type: 'doughnut',
    data: {
        datasets: [{
            label: 'Master medals',
            data: [50,120],
            text: "ff",
            backgroundColor: [
             
                'rgb(32,178,170)'
            ],
            borderWidth: 0
        }],
        labels: [
            'Master Medals',
        ]
    },
    options: {
        rotation: 1 * Math.PI,
        circumference: 1 * Math.PI,
        cutoutPercentage: 70,
        legend:{
            position:'top',
            labels: {
                usePointStyle: true,
                fontSize:11,
                boxWidth:6
            }
        }

    }    
}